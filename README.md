
Module: Multidomain Facebook Pixel
Author: Said El fazni <https://www.drupal.org/u/fazni>


Description
===========
This module implements a Facebook Tracking Pixel to track your users for site Multidomain.

Requirements
============

* Create account a Facebook pixel for each domain under https://www.facebook.com/ads/manager/pixel/


Installation
============
1) Download and copy the module in to module directory.
2) Activate the module.
3) Enter your Facebook Tracking ID in /admin/config/domain/domain_facebook_pixel/config.

TODO
-----------
* Make the trackable events configurable (Now only the "PageView" event is hardcoded available) -> Patches are welcome :)


Dependencies:
-------------
    -domain


MAINTAINERS
-----------

Current maintainers:
* fazni - https://www.drupal.org/u/fazni
